package practice.katas.rmenacho;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Example test class.
 */
public class ExampleKataSolutionTest {
  @Test
  public void testSumMethod() {
    ExampleKataSolution exampleInstance = new ExampleKataSolution();

    assertEquals(exampleInstance.sum(2, 2), 4);
  }

}
